# AcctInqRqAcctInqRq

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acct_sel** | Option<[**crate::models::AcctSel**](AcctSel.md)> |  | [optional]
**efx_hdr** | Option<[**crate::models::EfxHdr**](EFXHdr.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


