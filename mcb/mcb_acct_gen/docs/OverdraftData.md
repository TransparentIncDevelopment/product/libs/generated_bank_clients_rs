# OverdraftData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**atm_pos_over_draft** | Option<[**crate::models::AtmPosOverDraft**](AtmPosOverDraft.md)> |  | [optional]
**over_draft_type_code** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


