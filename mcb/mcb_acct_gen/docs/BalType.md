# BalType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bal_type_source** | Option<**String**> |  | [optional]
**bal_type_values** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


