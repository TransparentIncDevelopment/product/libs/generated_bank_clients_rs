# AcctRec

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acct_id** | Option<**String**> |  | [optional]
**acct_info** | Option<[**crate::models::AcctInfo**](AcctInfo.md)> |  | [optional]
**acct_status** | Option<[**crate::models::AcctStatus**](AcctStatus.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


