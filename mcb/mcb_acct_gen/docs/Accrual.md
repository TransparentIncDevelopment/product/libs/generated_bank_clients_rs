# Accrual

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accrual_frequency** | Option<[**crate::models::AccrualAccrualFrequency**](Accrual_AccrualFrequency.md)> |  | [optional]
**accrual_on_balance** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


