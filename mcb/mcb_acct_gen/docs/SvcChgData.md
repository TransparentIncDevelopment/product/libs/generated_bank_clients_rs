# SvcChgData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**svc_chg_acct_ref** | Option<[**crate::models::SvcChgAcctRef**](SvcChgAcctRef.md)> |  | [optional]
**svc_chg_flat_fee** | Option<[**crate::models::SvcChgFlatFee**](SvcChgFlatFee.md)> |  | [optional]
**svc_chg_method** | Option<[**Vec<crate::models::SvcChgMethod>**](SvcChgMethod.md)> |  | [optional]
**svc_chg_time_frame** | Option<[**crate::models::SvcChgTimeFrame**](SvcChgTimeFrame.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


