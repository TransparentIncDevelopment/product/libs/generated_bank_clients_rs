# AcctMemoData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acct_memo_code** | Option<**String**> |  | [optional]
**acct_memo_ident** | Option<**String**> |  | [optional]
**acct_memo_type** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


