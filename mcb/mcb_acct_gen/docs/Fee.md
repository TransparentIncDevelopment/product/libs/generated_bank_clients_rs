# Fee

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fee_ident** | Option<**String**> |  | [optional]
**fee_option** | Option<**String**> |  | [optional]
**fee_type** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


