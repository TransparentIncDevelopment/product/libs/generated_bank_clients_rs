# CurAmt

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amt** | Option<**String**> |  | [optional]
**cur_code** | Option<[**crate::models::CurCode**](CurCode.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


