# AtmPosOverDraft

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auth_limit_option** | Option<**String**> |  | [optional]
**opt_in_out_dt** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


