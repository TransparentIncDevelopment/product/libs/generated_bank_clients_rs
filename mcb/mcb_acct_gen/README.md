# Rust API client for mcb_acct_gen

IFX Services for Metropolitan

## Overview

This API client was generated by the [OpenAPI Generator](https://openapi-generator.tech) project.  By using the [openapi-spec](https://openapis.org) from a remote server, you can easily generate an API client.

- API version: 0.0.1
- Package version: 5.0.0
- Build package: org.openapitools.codegen.languages.RustClientCodegen

## Installation

Put the package under your project folder and add the following to `Cargo.toml` under `[dependencies]`:

```
    openapi = { path = "./generated" }
```

## Documentation for API Endpoints

All URIs are relative to *http://localhost:8091*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AcctServiceControllerApi* | [**account_inquiry_using_post**](docs/AcctServiceControllerApi.md#account_inquiry_using_post) | **POST** /Accounts | accountInquiry


## Documentation For Models

 - [Accrual](docs/Accrual.md)
 - [AccrualAccrualFrequency](docs/AccrualAccrualFrequency.md)
 - [AcctBal](docs/AcctBal.md)
 - [AcctInfo](docs/AcctInfo.md)
 - [AcctInqRq](docs/AcctInqRq.md)
 - [AcctInqRqAcctInqRq](docs/AcctInqRqAcctInqRq.md)
 - [AcctInqRs](docs/AcctInqRs.md)
 - [AcctInqRsAcctInqRs](docs/AcctInqRsAcctInqRs.md)
 - [AcctKeys](docs/AcctKeys.md)
 - [AcctMemoData](docs/AcctMemoData.md)
 - [AcctPeriodData](docs/AcctPeriodData.md)
 - [AcctRec](docs/AcctRec.md)
 - [AcctSel](docs/AcctSel.md)
 - [AcctStatus](docs/AcctStatus.md)
 - [AcctStmtData](docs/AcctStmtData.md)
 - [AcctType](docs/AcctType.md)
 - [AtmPosOverDraft](docs/AtmPosOverDraft.md)
 - [BalType](docs/BalType.md)
 - [CashMgmtRelData](docs/CashMgmtRelData.md)
 - [Client](docs/Client.md)
 - [CurAmt](docs/CurAmt.md)
 - [CurCode](docs/CurCode.md)
 - [DateData](docs/DateData.md)
 - [DepAcctData](docs/DepAcctData.md)
 - [EfxHdr](docs/EfxHdr.md)
 - [Fee](docs/Fee.md)
 - [IntDispData](docs/IntDispData.md)
 - [Organization](docs/Organization.md)
 - [OverdraftData](docs/OverdraftData.md)
 - [RecurRule](docs/RecurRule.md)
 - [RelationshipMgr](docs/RelationshipMgr.md)
 - [RelationshipPricingAcctData](docs/RelationshipPricingAcctData.md)
 - [Status](docs/Status.md)
 - [StmtTimeFrame](docs/StmtTimeFrame.md)
 - [SvcChgAcctRef](docs/SvcChgAcctRef.md)
 - [SvcChgData](docs/SvcChgData.md)
 - [SvcChgFlatFee](docs/SvcChgFlatFee.md)
 - [SvcChgFlatFeeAmt](docs/SvcChgFlatFeeAmt.md)
 - [SvcChgMethod](docs/SvcChgMethod.md)
 - [SvcChgTimeFrame](docs/SvcChgTimeFrame.md)
 - [SweepAcctKeys](docs/SweepAcctKeys.md)
 - [Tracking](docs/Tracking.md)


To get access to the crate's generated documentation, use:

```
cargo doc --open
```

## Author



