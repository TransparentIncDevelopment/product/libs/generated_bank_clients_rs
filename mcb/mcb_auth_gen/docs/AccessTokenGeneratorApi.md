# \AccessTokenGeneratorApi

All URIs are relative to *http://localhost:8091*

Method | HTTP request | Description
------------- | ------------- | -------------
[**generate_access_token_using_post**](AccessTokenGeneratorApi.md#generate_access_token_using_post) | **POST** /getAccessToken | generateAccessToken



## generate_access_token_using_post

> crate::models::AccessTokenOutput generate_access_token_using_post(grant_type, scope)
generateAccessToken

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**grant_type** | Option<**String**> |  |  |
**scope** | Option<**String**> |  |  |

### Return type

[**crate::models::AccessTokenOutput**](AccessTokenOutput.md)

### Authorization

[basic_auth](../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: application/x-www-form-urlencoded
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

