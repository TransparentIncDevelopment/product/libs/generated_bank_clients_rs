# XferReq

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**xfer_req** | Option<[**crate::models::XferReqXferReq**](XferReq_XferReq.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


