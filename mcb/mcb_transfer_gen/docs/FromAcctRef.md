# FromAcctRef

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acct_keys** | Option<[**crate::models::AcctKeys**](AcctKeys.md)> |  | [optional]
**trn_code** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


