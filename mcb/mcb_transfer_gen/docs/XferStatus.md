# XferStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acct_info_dtl** | Option<[**crate::models::AcctInfoDtl**](AcctInfoDtl.md)> |  | [optional]
**xfer_status_rec** | Option<[**crate::models::XferStatusRec**](XferStatusRec.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


