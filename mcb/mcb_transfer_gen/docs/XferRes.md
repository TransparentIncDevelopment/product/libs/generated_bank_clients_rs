# XferRes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**xfer_res** | Option<[**crate::models::XferResXferRes**](XferRes_XferRes.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


