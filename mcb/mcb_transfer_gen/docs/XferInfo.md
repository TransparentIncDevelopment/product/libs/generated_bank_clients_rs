# XferInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cur_amt** | Option<[**crate::models::CurAmt**](CurAmt.md)> |  | [optional]
**from_acct_ref** | Option<[**crate::models::FromAcctRef**](FromAcctRef.md)> |  | [optional]
**to_acct_ref** | Option<[**crate::models::ToAcctRef**](ToAcctRef.md)> |  | [optional]
**category** | Option<**String**> |  | [optional]
**xfer_from_desc** | Option<**String**> |  | [optional]
**xfer_to_desc** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


