# XferReqXferReq

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**efx_hdr** | Option<[**crate::models::EfxHdr**](EFXHdr.md)> |  | [optional]
**xfer_info** | Option<[**crate::models::XferInfo**](XferInfo.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


