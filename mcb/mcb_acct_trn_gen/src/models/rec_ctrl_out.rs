/*
 * IFX Services for Metropolitan
 *
 * IFX Services for Metropolitan
 *
 * The version of the OpenAPI document: 0.0.1
 *
 * Generated by: https://openapi-generator.tech
 */

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct RecCtrlOut {
    #[serde(rename = "Cursor", skip_serializing_if = "Option::is_none")]
    pub cursor: Option<String>,
    #[serde(rename = "MatchedRecCount", skip_serializing_if = "Option::is_none")]
    pub matched_rec_count: Option<String>,
    #[serde(rename = "RemainRecCount", skip_serializing_if = "Option::is_none")]
    pub remain_rec_count: Option<String>,
    #[serde(rename = "SentRecCount", skip_serializing_if = "Option::is_none")]
    pub sent_rec_count: Option<String>,
}

impl RecCtrlOut {
    pub fn new() -> RecCtrlOut {
        RecCtrlOut {
            cursor: None,
            matched_rec_count: None,
            remain_rec_count: None,
            sent_rec_count: None,
        }
    }
}
