/*
 * IFX Services for Metropolitan
 *
 * IFX Services for Metropolitan
 *
 * The version of the OpenAPI document: 0.0.1
 *
 * Generated by: https://openapi-generator.tech
 */

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct ChkNumRange {
    #[serde(rename = "ChkNumEnd", skip_serializing_if = "Option::is_none")]
    pub chk_num_end: Option<String>,
    #[serde(rename = "ChkNumStart", skip_serializing_if = "Option::is_none")]
    pub chk_num_start: Option<String>,
}

impl ChkNumRange {
    pub fn new() -> ChkNumRange {
        ChkNumRange {
            chk_num_end: None,
            chk_num_start: None,
        }
    }
}
