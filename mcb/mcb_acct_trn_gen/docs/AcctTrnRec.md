# AcctTrnRec

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acct_trn_id** | Option<**String**> |  | [optional]
**acct_trn_info** | Option<[**crate::models::AcctTrnInfo**](AcctTrnInfo.md)> |  | [optional]
**acct_trn_status** | Option<[**crate::models::AcctTrnStatus**](AcctTrnStatus.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


