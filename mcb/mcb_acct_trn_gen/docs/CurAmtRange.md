# CurAmtRange

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**high_cur_amt** | Option<[**crate::models::HighCurAmt**](HighCurAmt.md)> |  | [optional]
**low_cur_amt** | Option<[**crate::models::LowCurAmt**](LowCurAmt.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


