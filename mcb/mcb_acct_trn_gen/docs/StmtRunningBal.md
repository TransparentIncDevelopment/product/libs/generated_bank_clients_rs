# StmtRunningBal

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amt** | Option<**f32**> |  | [optional]
**cur_code** | Option<[**crate::models::CurCode**](CurCode.md)> |  | [optional]
**stmt_running_bal_type** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


