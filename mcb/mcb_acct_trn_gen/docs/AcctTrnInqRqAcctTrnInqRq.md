# AcctTrnInqRqAcctTrnInqRq

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acct_trn_sel** | Option<[**crate::models::AcctTrnSel**](AcctTrnSel.md)> |  | [optional]
**efx_hdr** | Option<[**crate::models::EfxHdr**](EFXHdr.md)> |  | [optional]
**rec_ctrl_in** | Option<[**crate::models::RecCtrlIn**](RecCtrlIn.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


