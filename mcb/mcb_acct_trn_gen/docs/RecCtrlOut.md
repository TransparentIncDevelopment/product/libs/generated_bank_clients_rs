# RecCtrlOut

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cursor** | Option<**String**> |  | [optional]
**matched_rec_count** | Option<**String**> |  | [optional]
**remain_rec_count** | Option<**String**> |  | [optional]
**sent_rec_count** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


