# AcctTrnSel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acct_keys** | Option<[**crate::models::AcctKeys**](AcctKeys.md)> |  | [optional]
**branch_ident** | Option<**String**> |  | [optional]
**chk_num_range** | Option<[**crate::models::ChkNumRange**](ChkNumRange.md)> |  | [optional]
**cur_amt_range** | Option<[**crate::models::CurAmtRange**](CurAmtRange.md)> |  | [optional]
**dt_range** | Option<[**Vec<crate::models::DtRange>**](DtRange.md)> |  | [optional]
**period_type** | Option<**String**> |  | [optional]
**sort_order** | Option<**String**> |  | [optional]
**teller_idet** | Option<**String**> |  | [optional]
**trn_src** | Option<**String**> |  | [optional]
**trn_type** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


