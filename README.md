- [Generating Client code from Swagger](#generating-client-code-from-swagger)
    - [Generating Client from OpenAPI Spec](#generating-client-from-openapi-spec)
      - [Define and Generate a New Client](#define-and-generate-a-new-client)
      - [Regenerate a Client](#regenerate-a-client)
      - [Format Generated Code](#format-generated-code)
    - [Importing Generated Code](#importing-generated-code)
      - [Metropolitan Commercial Bank (MCB)](#metropolitan-commercial-bank-mcb)
      - [Provident Bank via Treasury Prime API](#provident-bank-via-treasury-prime-api)
    - [Versioning Crates and API](#versioning-crates-and-api)

## Generating Client code from Swagger
Given a swagger spec, this doc outlines the steps to generate the corresponding client code.

#### Generating Client from OpenAPI Spec

Given an OpenApi spec, one can generate ([link to tool](https://github.com/OpenAPITools/openapi-generator)) a Rust client with 
request/response data contracts (serde serializable) for the API endpoints, traits that represent a strongly typed 
client mapping functions to the API endpoints, and also the http client itself. The result is a standalone
crate, exporting those strongly typed artifacts. The generator tool has [config options](https://github.com/OpenAPITools/openapi-generator/blob/master/docs/generators/rust.md) 
to produce a client using `reqwest` or `hyper`.

##### Define and Generate a New Client
1) Locate swagger/openapi doc for target API ([Rust](https://openapi-generator.tech/docs/generators/rust))
2) Pick the destination for generated code
3) Test the Swagger definition using [Swagger Editor](https://editor.swagger.io/)
4) Run generator tool (using [Docker](https://hub.docker.com/r/openapitools/openapi-generator-cli); Java options also available)
5) TEMPORARY: Rust format the code (see below)

See the `./mcb_client_adapter/generate_raw_client.sh` script for an example on how to generate a client. 

In the generation script, add these two lines at the top of the `lib.rs` file to accomodate clippy:
```rust
#![allow(clippy::all)]
#![allow(bare_trait_objects)]
```
For example:

```sh
# Prepend Rust boilerplate to make generated client ignored by clippy
sed -i '1i #![allow(clippy::all)]\n#![allow(bare_trait_objects)]' ./treasury_prime_client/src/lib.rs
```

##### Regenerate a Client

**MCB**
```bash
./mcb_inputs/generate_raw_client.sh
```

**Provident via Treasury Prime API**
```bash
./treasury_prime/generate_client.sh
```

##### Format Generated Code
**TEMPORARY**:
`rustfmt` offers an [ignore option](https://github.com/rust-lang/rustfmt/blob/master/Configurations.md#ignore),
 which we could add to a `rustfmt.toml` flag at the generated directory root. However, `cargo fmt` doesn't 
 respect the `ignore` option unless it is nightly. This is used to prevent forcing everyone to use `cargo +nightly fmt --all`,
 format the generated code in order to pass our CI build pipeline.
 
 To Rust format the generated client, run:
 ```shell script
cargo fmt --all
```

#### Importing Generated Code

##### Metropolitan Commercial Bank (MCB)

By convention, the client adapter crate stores the `openapi.yaml` spec of the target API in its root directory.
The generated directory will be under `/generated/<PREFIX>_gen` matching the name for the client http adapter crate `PREFIX_client_adapter`.

Any client we build can reference the generated types by adding a reference to the generated crate 
in its `Cargo.toml` using the relative path:

```toml
[dependencies.mcb_gen]
package = 'mcb_gen'
path = "../generated/mcb_gen"
```

By default, openapi names the generated package as `openapi`, so you can rename it when you import 
it to something more locally sensible.

Import the generated stuffs with
```rust
use mcb_gen::api::APIClient;
use mcb_gen::models::AccountBalanceRequest;
use mcb_gen::models::AccountBalanceResponse;

etc. etc.
```

##### Provident Bank via Treasury Prime API

The client crate adapter stores a `treasury_api.yaml` spec of the target API in its root directory.
The generated directory will be under `/treasury_prime_client/` matching the name for the client http adapter crate `treasury_prime_client_adapter`.

Any client we build can reference the generated types by adding a reference to the generated crate 
in its `Cargo.toml` using the relative path:

```toml
[dependencies.treasury_prime_client]
package = 'treasury_prime_client'
path = "../generated/treasury_prime/treasury_prime_client"
```

And then using the generated types in the crate:

```rust
use treasury_prime_client::models::book_transfer::Status;
```

#### Versioning Crates and API

To version the generated rust crates, change the `VERSION` variable in the associated client generation script.
**Example**:  To version the generated mcb rust crate `mcb_auth_gen` as `1.1.0`, edit `mcb_inputs/generate_raw_client.sh` to define `VERSION` as `1.1.0` in that crate's section in the script.

To version the API itself, edit its .yaml file's `info:version` config.  
**Example**: To version the treasure prime API to `2.0.0`, edit `treasury_prime/treasury_api.yaml` to `2.0.0` under `info:version` at the top of the file instead of its previous value.