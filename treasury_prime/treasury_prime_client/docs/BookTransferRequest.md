# BookTransferRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **String** |  | 
**description** | Option<**String**> |  | [optional]
**from_account_id** | **String** |  | 
**to_account_id** | **String** |  | 
**userdata** | [**crate::models::BookTransferRequestUserdata**](BookTransferRequest_userdata.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


